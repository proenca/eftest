/*
**  Main funcionality for the slider
*/

$('document').ready(function(){

  // Initialization
  var $el = $('.slider > ul');
  var i = 0;
  var imgwidth = $('.slider').width();
  var translation = 0;
  var maxsize = $el.find('li').length;
  var slideinterval = 2500;
  var slideIntStore = false;

  // Sets Slider dots (one dot for each image)
  for(f=0; f<maxsize; f++){
    $('.sliderdots').append('<div class="dot"></div>');
  }
  $('.sliderdots > div').eq(0).addClass('active'); // Initiates first dot as active

  $el.find('li').width(imgwidth); // Initiates images width

  if(!Modernizr.csstransitions) // Initiates the margin-left to 0, so the first animation works correctly
    $el.css('margin-left', '0');

  // Starts the slider
  setSliderInterval();

/*
  Updates image sizes when window is resized, also turns off the transition
  while updates the translate of the slider, this way we don't have a slow
  animation while the window size is changing
*/
  $(window).resize(function(){
    imgwidth = $('.slider').width();
    $el.find('li').width(imgwidth);

    updateSliderPosition(false);
  })

  // Click event for the slider dots, resets the timer, and updates the slider pos
  $('.sliderdots').on('click', 'div',function(ev){
    i = $(ev.target).index();
    slideOnePos();
  });

  // Arrow events for the slider
  $('.sliderwrapper').on('click','.sliderarrow', function(ev){
    if($(this).hasClass("slideleft")) i--;
    else i++;

    slideOnePos();
  });

  // Touch events for the slider
  $('.slider').on('swiperight', function(){
    i--;
    slideOnePos();
  });

  $('.slider').on('swipeleft', function(){
    i++;
    slideOnePos();
  });

  function slideOnePos(){
    // Checks if the slider position is correct, and corrects it
    if(i < 0) i = maxsize-1;
    if(i >= maxsize) i = 0;
    setSliderInterval();
    updateSliderPosition(true);
  }

  function setSliderInterval(){
    if(slideIntStore){
      clearInterval(slideIntStore);
    }
    slideIntStore = setInterval(function () {
      i++;
      if(i >= maxsize) i = 0;
      //$el.css('transition-duration', '0.5s');
      updateSliderPosition(true);
    }, slideinterval);
  }

  function updateSliderPosition(animate){
    // If the browser doesn't support transform, use jQuery animation
    if(Modernizr.csstransitions){
      if(animate){
        $el.css('transition-duration', '0.5s');
      }else{
        $el.css('transition-duration', '0s');
      }
      $el.css('transform','translate3d(-'+i*imgwidth+'px,0px,0px)');
    }
    else {
      if(animate){
        $el.animate(
          {
            'margin-left': -(i*imgwidth)
          },{
            queue: false,
            duration: 500
          });
      }else{
        $el.css('margin-left', -(i*imgwidth));
      }
    }

    $('.sliderdots').find('.active').removeClass('active');
    $('.sliderdots > div').eq(i).addClass('active');
  }
});
